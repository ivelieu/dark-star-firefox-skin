# Dark Star firefox skin

![preview image 1](showcase/darkstar_1.png)

![preview image 1](showcase/darkstar_2.png)

This is the skin I use for firefox. I use it with this firefox theme: https://addons.mozilla.org/en-US/firefox/addon/dark-matter-purple by BobbyKat LittleCub. 

Modified from https://github.com/sagars007/starry-fox by sagars007, and I added some quality of life features by MrOtherGuy. I removed CSS code from sagars007 that I did not personally find useful. Credit to both sagars007 and BobbyKat LittleCub. Of course, you could easily change the gradient settings to work with a different firefox theme too.

See the instructions at https://www.howtogeek.com/334716/how-to-customize-firefoxs-user-interface-with-userchrome.css/, for example, to install the skin. 

One of the main goals in making is to get a zero pixel gap between the tab bar and the nav bar. This took some fiddling. I also added a one pixel gradient in between tab markers. The overall effect of this makes it look considerably different to starry-fox. When opening the navbar to type an entry, I changed the style somewhat and added an offset on the right end so that the gradient covers the 'next' and 'settings' buttons. 

I also added easy customisation options to change color gradients used throughout the skin in the top of the userChrome.css file, something which starry-fox does not have. These are the settings I use:

```
:root {
	--bright-menu-color: 226, 175, 255;
	--darker-menu-color: #F067FF;
	/*--darker-menu-color: #9400ff;*/

	/* This is the base color the gradient is applied on top of for the navbar and tabs. */
	--dark-tab-background-color: 17, 17, 17;

	/* The main colors which change the theme. */
	--theme-gradient-full-opacity: linear-gradient(90deg, rgba(252,0,140,1) 0%, rgba(255,0,249,1) 30%, rgba(106,0,255,1) 100%);
	--theme-gradient-thirty-opacity: linear-gradient(90deg, rgb(252,0,140,0.3) 0%, rgb(255,0,249,0.3) 30%, rgb(106,0,255,0.3) 100%);
	--theme-gradient-seventy-opacity: linear-gradient(90deg, rgba(252,0,140,0.7) 0%, rgba(255,0,249,0.7) 30%, rgba(106,0,255,0.7) 100%);
}
```
Note that I use this with 1080 pixel width monitors, some variables may need modest changes for the navbar to align correctly, ie. on this line, just change it to something that looks nice instead of the 70% I use:

```
#urlbar-container {
    flex: 0 0 70% !important; /*sucks up some percentage of the row */
}
```

As of the time of uploading, I have used the skin for a year on multiple desktop environments with no issues. Feel free to post an issue if it does not render correctly after fiddling with the urlbar-container size as described above.

## License
Licensed under Mozilla Public License 2.0.
